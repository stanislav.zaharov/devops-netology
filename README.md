# devops-netology

�����, ������� ����� ��������������� �������� ������������ .\terraform\.gitignore (��� ������� ��������� � ������� "git check-ignore")

**/.terraform/* - ���������� �� ����� .terraform � �����

*.tfstate - ��� ����� � ����������� tfstate

*.tfstate.* - ��� ����� � ��������� tfstate

crash.log - ����������� ���� - �����

*.tfvars - ��� ����� � ����������� tfvars

override.tf - ����������� ���� - �����

override.tf.json - ����������� ���� - �����

*_override.tf - ��� ����� ��������������� �� _override.tf

*_override.tf.json - ��� ����� ��������������� �� _override.tf.json

.terraformrc - ����������� ���� - �����

terraform.rc - ����������� ���� - �����
